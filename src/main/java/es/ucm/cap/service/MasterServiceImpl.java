package es.ucm.cap.service;

import es.ucm.cap.facade.MasterFacade;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class MasterServiceImpl implements MasterService {

  @Autowired
  private MasterFacade masterFacade;

  @Override
  public ResponseEntity<?> calculate(int num) {
    long startTime = System.currentTimeMillis();

    Map<Integer, Long> result = new HashMap<Integer, Long>();
    IntStream.range(0, num).parallel().forEach(i -> result.put(i, masterFacade.calculate(i)));
    long endTime = System.currentTimeMillis();
    result.put(-1, endTime - startTime);
    return new ResponseEntity<Map<Integer, Long>>(result, HttpStatus.OK);
  }

}
