package es.ucm.cap.service;

import org.springframework.http.ResponseEntity;

public interface MasterService {

  public ResponseEntity<?> calculate(int num);

}
