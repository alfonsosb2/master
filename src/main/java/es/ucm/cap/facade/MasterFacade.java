package es.ucm.cap.facade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class MasterFacade {

  @Value("${node.url}")
  private String url;

  @Autowired
  private RestTemplate restTemplate;

  public Long calculate(int num) {
    return restTemplate.getForObject(url, Long.class, num);
  }

}
