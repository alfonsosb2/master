package es.ucm.cap.controller;

import es.ucm.cap.service.MasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MasterController {

  @Autowired
  private MasterService masterService;

  @RequestMapping(value = "/master/calculate/{num}", method = RequestMethod.GET)
  public ResponseEntity<?> calculate(@PathVariable Integer num) {
    return masterService.calculate(num);
  }
}
